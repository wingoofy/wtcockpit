#ifndef MAINWINDOW_H
#define MAINWINDOW_H

//TODO Input mező a frissítés gyakoriságának a beállítására.
//TODO water temp / oil temp értékekek hozzáadása.
//TODO IAS műszer grafikus megjelenítése.

#include <QMainWindow>
#include <QQuickWidget>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QTimer>
#include "aircraft.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

#define STATE 1
#define INDICATORS 2

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:

    void updateFlightData(char source);
    void on_btnGet_clicked();
    void getFlightData();

private:

    Ui::MainWindow *ui;
    QNetworkAccessManager *manager;
    QNetworkRequest requestState;
    QNetworkRequest requestIndicators;
    QNetworkReply *reply;
    QTimer* updateTimer;
    char lastRequest;
    Aircraft* aircraft;
};
#endif // MAINWINDOW_H
