#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    requestState.setUrl(QUrl("http://localhost:8111/state"));
    requestIndicators.setUrl(QUrl("http://localhost:8111/indicators"));
    lastRequest = 0;
    aircraft = new Aircraft();

    manager = new QNetworkAccessManager();
    QObject::connect(manager, &QNetworkAccessManager::finished,
                     this, [=](QNetworkReply *reply) {
        if (reply->error()) {
            qDebug() << reply->errorString();
            return;
        }
        if (lastRequest == STATE)
        {
            updateFlightData(STATE);
            getFlightData();
        } else updateFlightData(INDICATORS);
    }
    );
}

void MainWindow::updateFlightData(char source)
{
    QString answer = reply->readAll();
    QJsonDocument jsonFlightDataDoc =  QJsonDocument::fromJson(answer.toUtf8());
    QJsonObject jsonFlightData = jsonFlightDataDoc.object();

    if (source == STATE)
    {
        //qDebug() << jsonFlightData["IAS, km/h"];
        aircraft->IAS = jsonFlightData["IAS, km/h"].toInt();
        //qDebug() << jsonFlightData["H, m"];
        aircraft->ALT = jsonFlightData["H, m"].toInt();

            //IAS
            if (aircraft->IAS < 150)
                ui->pbIAS->setStyleSheet("QProgressBar::chunk {background: red}");
            else if (aircraft->IAS < 200)
                ui->pbIAS->setStyleSheet("QProgressBar::chunk {background: yellow}");
            else ui->pbIAS->setStyleSheet("QProgressBar::chunk {background: green}");
            ui->pbIAS->setValue(aircraft->IAS);
            ui->lblIAS->setText(QString::number(aircraft->IAS));

            //ALT
            if (aircraft->ALT < 1000)
                ui->pbALT->setStyleSheet("QProgressBar::chunk {background: yellow}");
            else
                ui->pbALT->setStyleSheet("QProgressBar::chunk {background: green}");
            ui->lblALT->setText(QString::number(aircraft->ALT));
            ui->pbALT->setValue(aircraft->ALT);

    } else
    {
        //qDebug() << jsonFlightData["compass"];
        aircraft->HDG = (int) jsonFlightData["compass"].toDouble();
        //qDebug() << (int) hdg;

            //HDG
            ui->lblHDG->setText(QString::number(aircraft->HDG));
            ui->pbHDG->setValue(180 - aircraft->HDG);
    }
}

MainWindow::~MainWindow()
{
    delete ui;
    delete manager;
}

void MainWindow::on_btnGet_clicked()
{
    updateTimer = new QTimer(this);
    updateTimer->start(200);

    connect(updateTimer, SIGNAL(timeout()), this, SLOT(getFlightData()));
}

void MainWindow::getFlightData()
{
    if (lastRequest != STATE)
    {
        lastRequest = STATE;
        reply = manager->get(requestState);
    } else
    {
        lastRequest = INDICATORS;
        reply = manager->get(requestIndicators);
    }

}
