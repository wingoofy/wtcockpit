import QtQuick 2.0

Item {
    width: 264
    height: 264
    Image {
                id: background
                width: 264
                height: 264
                anchors.fill: parent
                source: ""
                fillMode: Image.PreserveAspectCrop

                Image {
                    id: image
                    x: 0
                    y: 0
                    source: "resources/gauge_background.png"
                    fillMode: Image.PreserveAspectFit
                }
            }
}
